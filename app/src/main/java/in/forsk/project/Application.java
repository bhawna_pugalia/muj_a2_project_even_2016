package in.forsk.project;

import org.acra.annotation.ReportsCrashes;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;

@ReportsCrashes(formKey = "", // This is required for backward compatibility but
								// not used
formUri = "https://collector.tracepot.com/3553b3fd")

//@ReportsCrashes(formKey = "", // will not be used
//mailTo = "saurabh@neuerung.co.in", customReportContent = {
//		ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
//		ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
//		ReportField.CUSTOM_DATA, ReportField.STACK_TRACE }, mode = ReportingInteractionMode.TOAST, resToastText = R.string.app_name)
public class Application extends android.app.Application

{
	
	public static Application context;

	public static int mDeviceWidth = 0;
	public static int mDeviceHeight = 0;

	@Override
	public void onCreate() {
		super.onCreate();

		context = this;
		/*
		 * AQUtility.setDebug(false);
		 * 
		 * ACRA.init(this);
		 */

//		AQUtility.setDebug(true);
//		AQUtility.setCacheDir(getExternalCacheDir());

		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		mDeviceWidth = displayMetrics.widthPixels;
		mDeviceHeight = displayMetrics.heightPixels;
		
//		PreferenceManager.getInstance(context);
	}

	public static boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null) {
				if (info.isConnected() && (info.getType() == connectivity.TYPE_ETHERNET
						|| info.getType() == connectivity.TYPE_MOBILE || info.getType() == connectivity.TYPE_WIFI)) {
					return true;
				}
			}

		}
		return false;
	}

}
